package nixpkgs

import "testing"

func TestShouldUpdate(t *testing.T) {
	c, err := Default.getCommitGit()
	if err != nil {
		t.Fatal(err)
	}

	if c == "" {
		t.Fatal("Failed to fetch commit")
	}

	println(c)
}
