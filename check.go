package nixpkgs

import (
	"io/ioutil"

	"github.com/valyala/fastjson"
)

func (e Endpoint) getCommitGit() (string, error) {
	r, err := Client.Get(e.GitBranch)
	if err != nil {
		return "", err
	}

	defer r.Body.Close()

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return "", err
	}

	return fastjson.GetString(body, "commit", "sha"), nil
}

// ShouldUpdate checks the upstream repository for the commit hash
func (e Endpoint) ShouldUpdate(commit string) bool {
	c, err := e.getCommitGit()
	if err != nil {
		return true
	}

	return c == commit
}
