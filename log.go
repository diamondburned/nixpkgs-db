package nixpkgs

import "log"

// Verbose when true, logs a lot more things. The logger
// used is the stdlib log
var Verbose = false

// VerboseLog logs, if Verbose is true
func VerboseLog(i interface{}) {
	if Verbose {
		log.Printf("[nixpkgs] Verbose: %#v\n", i)
	}
}

// Error logs all errors, regardless of Verbose
func Error(err error) {
	log.Printf("[nixpkgs] Error: %#v\n", err)
}
