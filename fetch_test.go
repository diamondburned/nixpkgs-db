package nixpkgs

import (
	"testing"

	"github.com/k0kubun/pp"
)

func TestFetchRepository(t *testing.T) {
	r, err := Default.fetchRepository()
	if err != nil {
		t.Fatal(err)
	}

	gp, ok := r.Packages["go_1_12"]
	if !ok {
		t.Fatal("go_1_12 not found")
	}

	pp.Println(gp)
}
