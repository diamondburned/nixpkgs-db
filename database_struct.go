package nixpkgs

// Package holds a package metadata. Some fields are
// omitted due to Nix's JSON inconsistencies.
type Package struct {
	NixID  string `storm:"unique,id"`
	Name   string
	System string
	Meta   Meta
}

type Meta struct {
	Available   bool
	Name        string
	Description string
	Position    string
}

func convertPkgs(pkgs map[string]*pkg) []*Package {
	packages := make([]*Package, 0, len(pkgs))
	for id, p := range pkgs {
		packages = append(packages, &Package{
			NixID:  id,
			Name:   p.Name,
			System: p.System,
			Meta:   p.Meta,
		})
	}

	return packages
}
