package nixpkgs

import (
	"encoding/json"
	"sync"
)

// Repository is the root of the repository.
type repository struct {
	Commit   string          `json:"commit"`
	Packages map[string]*pkg `json:"packages"`
}

type pkg struct {
	Name   string `json:"name" storm:"unique"`
	System string `json:"system"`
	Meta   Meta   `json:"meta"`
}

var mu sync.Mutex

// fetchRepository fetches the repository and parses it.
func (e Endpoint) fetchRepository() (*repository, error) {
	// Prevent spamming calls
	mu.Lock()
	defer mu.Unlock()

	r, err := Client.Get(e.JSONURL)
	if err != nil {
		return nil, err
	}

	defer r.Body.Close()

	var repo = &repository{}
	if err := json.NewDecoder(r.Body).Decode(repo); err != nil {
		return nil, err
	}

	return repo, nil
}
