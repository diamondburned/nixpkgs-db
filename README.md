# [nixpkgs-db](https://godoc.org/gitlab.com/diamondburned/nixpkgs-db)

A caching Nixpkgs database library for Golang, taken from nixpkgs' `packages.json`.

`packages.json` does not contain any proprietary packages by default.

## How it works

The package works in a singleton mode, storing a `stormdb` file in the temporary directory. This way, the database is persistent between changes.

## How to use it

```go
import "gitlab.com/diamondburned/nixpkgs-db"
```

```go
// the `true` boolean enables periodic updates
if err := nixpkgs.Initialize(true); err != nil {
	t.Fatal(err)
}

pkgs, commit, err := nixpkgs.GetPackages()
if err != nil {
	t.Fatal(err)
}

fmt.Println(commit)

sort.Sort(pkgs)

// ...
```

For more examples, refer to `_test.go` files.

