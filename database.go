package nixpkgs

import (
	"errors"
	"os"
	"time"

	"github.com/asdine/storm"
	"github.com/asdine/storm/codec/protobuf"
)

// database singleton
var db *storm.DB

// DatabasePath is the path for the database cache
var DatabasePath = os.TempDir() + "/nixpkgs.db"

var (
	// ErrUninitialized is returned when Initialized() isn't called
	ErrUninitialized = errors.New("Singleton unitialized, call Initialize()")
)

var ticker *time.Ticker

// TickDuration is the update frequency
var TickDuration = time.Hour

// Initialize starts the database singleton and renew it
func Initialize(e Endpoint, schedule bool) (err error) {
	if db != nil {
		return nil
	}

	db, err = storm.Open(
		DatabasePath,
		storm.Batch(), storm.Codec(protobuf.Codec),
	)

	if err != nil {
		return
	}

	db.From(e.Name).From("packages").Init(&Package{})

	if schedule {
		go func() {
			if ticker != nil {
				ticker.Stop()
			}

			ticker = time.NewTicker(TickDuration)

			for {
				if _, ok := <-ticker.C; !ok {
					return
				}

				if err := e.Refresh(false); err != nil {
					Error(err)
				}
			}
		}()
	}

	return e.Refresh(true)
}

// Stop stops auto-updating, if it's scheduled.
func Stop() {
	if ticker != nil {
		ticker.Stop()
	}
}

// Refresh syncs the JSON with the database. If force is true,
// Refresh will always fetch the repository
func (e Endpoint) Refresh(force bool) (err error) {
	if db == nil {
		return ErrUninitialized
	}

	var r *repository

	// Check if the new commit differs from the old one. This would fail
	// if Refresh is being ran for the first time.
	// If force is true, this whole block would be skipped.
	if oldCommit := ""; db.Get(e.Name, "commit", &oldCommit) != nil && !force {
		if !e.ShouldUpdate(oldCommit) {
			return nil
		}

		r, err = e.fetchRepository()
		if err != nil {
			return err
		}

		if oldCommit == r.Commit {
			return nil
		}
	}

	if r == nil {
		r, err = e.fetchRepository()
		if err != nil {
			return err
		}
	}

	pkgs := convertPkgs(r.Packages)

	if err := db.Set(e.Name, "commit", r.Commit); err != nil {
		return err
	}

	return tx(e.Name, func(node storm.Node) error {
		node = node.From("packages")

		for _, p := range pkgs {
			if err := node.Save(p); err != nil {
				return err
			}
		}

		return nil
	})
}
