package nixpkgs

// Packages wraps around a slice of packages, used for
// easier methods.
type Packages []*Package

// GetPackages returns the packages, the commit string and the
// error, if there's any. Note that this strictly takes from
// the cache only.
func (e Endpoint) GetPackages() (Packages, string, error) {
	if db == nil {
		return nil, "", ErrUninitialized
	}

	var (
		pkgs   []*Package
		commit string
	)

	// This shouldn't ever fail. If this fails, the clause
	// below will fail as well.
	if err := db.Get(e.Name, "commit", &commit); err != nil {
		return nil, "", err
	}

	if err := db.From(e.Name).From("packages").All(&pkgs); err != nil {
		return nil, "", err
	}

	return Packages(pkgs), commit, nil
}

// Filter filters the Nix IDs for the wanted packages
func (p Packages) Filter(f func(p *Package) bool) Packages {
	var filter []*Package

	for _, p := range p {
		if f(p) {
			filter = append(filter, p)
		}
	}

	return filter
}

// These methods exist to satisfy sort.Sort(Packages)

// Len exists to satisfy sort.Interface
func (p Packages) Len() int {
	return len(p)
}

// Less exists to satisfy sort.Interface
func (p Packages) Less(i, j int) bool {
	return p[i].Name < p[j].Name
}

// Swap exists to satisfy sort.Interface
func (p Packages) Swap(i, j int) {
	p[i], p[j] = p[j], p[i]
}
