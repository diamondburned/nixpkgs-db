package nixpkgs

import "github.com/asdine/storm"

func tx(from string, f func(storm.Node) error) error {
	if db == nil {
		return ErrUninitialized
	}

	n, err := db.From(from).Begin(true)
	if err != nil {
		return err
	}

	defer n.Rollback()

	if err := f(n); err != nil {
		return err
	}

	return n.Commit()
}
