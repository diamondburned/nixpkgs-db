package nixpkgs

import (
	"net/http"
)

// Client is used for GET requests over the default client.
var Client = http.Client{
	Timeout: TickDuration,
}
