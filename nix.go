package nixpkgs

type Endpoint struct {
	Name      string
	JSONURL   string
	GitBranch string
}

var (
	NixOS19_03 = Endpoint{
		Name:      "NixOS 19.03",
		JSONURL:   "https://nixos.org/nixpkgs/packages-nixos-19.03.json.gz",
		GitBranch: "https://api.github.com/repos/NixOS/nixpkgs-channels/branches/nixos-19.03",
	}

	NixOSUnstable = Endpoint{
		Name:      "NixOS Unstable",
		JSONURL:   "https://nixos.org/nixpkgs/packages-nixos-unstable.json.gz",
		GitBranch: "https://api.github.com/repos/NixOS/nixpkgs-channels/branches/nixos-unstable",
	}

	NixpkgsUnstable = Endpoint{
		Name:      "Nixpkgs Unstable",
		JSONURL:   "https://nixos.org/nixpkgs/packages-nixpkgs-unstable.json.gz",
		GitBranch: "https://api.github.com/repos/NixOS/nixpkgs-channels/branches/nixpkgs-unstable",
	}

	Default = NixOS19_03
)
