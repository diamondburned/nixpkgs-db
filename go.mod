module gitlab.com/diamondburned/nixpkgs-db

require (
	github.com/asdine/storm v2.1.2+incompatible
	github.com/golang/protobuf v1.3.1 // indirect
	github.com/k0kubun/pp v3.0.1+incompatible
	github.com/manifoldco/promptui v0.3.2
	github.com/mattn/go-colorable v0.1.1 // indirect
	github.com/valyala/fastjson v1.4.1
	go.etcd.io/bbolt v1.3.2 // indirect
)
