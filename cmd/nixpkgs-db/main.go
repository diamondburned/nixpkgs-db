package main

import (
	"errors"
	"log"
	"strings"

	"github.com/k0kubun/pp"
	"github.com/manifoldco/promptui"
	"gitlab.com/diamondburned/nixpkgs-db"
)

func main() {
	log.Println("Fetching packages...")

	if err := nixpkgs.Initialize(true); err != nil {
		panic(err)
	}

	pkgs, commit, err := nixpkgs.GetPackages()
	if err != nil {
		panic(err)
	}

	log.Println("Database commit: " + commit)

	validate := func(input string) error {
		pkgs := pkgs.Filter(func(p *nixpkgs.Package) bool {
			if strings.Contains(p.NixID, input) || strings.Contains(p.Name, input) {
				return true
			}
			return false
		})

		if len(pkgs) == 0 {
			return errors.New("Package not found")
		}

		return nil
	}

	prompt := promptui.Prompt{
		Label:    "Package name",
		Validate: validate,
	}

	r, err := prompt.Run()
	if err != nil {
		panic(err)
	}

	pkgs = pkgs.Filter(func(p *nixpkgs.Package) bool {
		if strings.Contains(p.NixID, r) || strings.Contains(p.Name, r) {
			return true
		}
		return false
	})

	for _, p := range pkgs {
		pp.Println(p)
	}
}
