package nixpkgs

import (
	"fmt"
	"sort"
	"testing"

	"github.com/k0kubun/pp"
)

func TestDatabase(t *testing.T) {
	if err := Initialize(Default, false); err != nil {
		t.Fatal(err)
	}

	pkgs, commit, err := Default.GetPackages()
	if err != nil {
		t.Fatal(err)
	}

	fmt.Println(commit)

	sort.Sort(pkgs)

	fmt.Println("Printing first 3 packages, sorted")

	for i := 0; i < 3 && i < len(pkgs); i++ {
		pp.Println(pkgs[i])
	}

	fmt.Println("Filtering the Go 1.12 package")

	pkgs = pkgs.Filter(func(p *Package) bool {
		return p.NixID == "go_1_12"
	})

	if len(pkgs) < 1 {
		t.Fatal("go_1_12 not found")
	}

	pp.Println(pkgs[0])
}
